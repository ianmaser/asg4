// $Id: graphics.h,v 1.9 2014-05-15 16:42:55-07 - - $

#ifndef __GRAPHICS_H__
#define __GRAPHICS_H__

#include <memory>
#include <vector>
using namespace std;

#include <GL/freeglut.h>

#include "rgbcolor.h"
#include "shape.h"

enum Direction {UP, RIGHT, LEFT, DOWN};

class object {
   public:
      shared_ptr<shape> pshape;
      vertex center;
      rgbcolor color;
   
      // Default copiers, movers, dtor all OK.
      void draw() { 
        glLineWidth(1);
        pshape->draw (center, color);
      }
      void drawBorder(const rgbcolor& borderColor, GLfloat size) {
        glLineWidth(size);
        pshape->draw (center, borderColor);
      }
      void move (GLfloat delta_x, GLfloat delta_y) {
         center.xpos += delta_x;
         center.ypos += delta_y;
      }
};

class mouse {
      friend class window;
   private:
      int xpos {0};
      int ypos {0};
      int entered {GLUT_LEFT};
      int left_state {GLUT_UP};
      int middle_state {GLUT_UP};
      int right_state {GLUT_UP};
   private:
      void set (int x, int y) { xpos = x; ypos = y; }
      void state (int button, int state);
      void draw();
};


class window {
      friend class mouse;
   private:
      static int width;         // in pixels
      static int height;        // in pixels
      static vector<object> objects;
      static size_t selected_obj;
      // moveDelta: how far to move a shape when a move key is pressed
      static GLfloat moveDelta;
      static GLfloat borderSize;
      static rgbcolor borderColor;
      static mouse mus;
   private:
      static void close();
      static void entry (int mouse_entered);
      static void display();
      static void reshape (int width, int height);
      static void keyboard (GLubyte key, int, int);
      static void special (int key, int, int);
      static void motion (int x, int y);
      static void passivemotion (int x, int y);
      static void mousefn (int button, int state, int x, int y);
   public:
      static void push_back (const object& obj) {
                  objects.push_back (obj); }
      static void setwidth (int width_) { width = width_; }
      static void setheight (int height_) { height = height_; }
      static void set_border (const rgbcolor &color, GLfloat size) {
            borderColor = color;
            borderSize = size;
      }
      static void set_moveby (GLfloat mb) { moveDelta = mb; }
      static void move_selected_object(Direction D);
      static void main();
};

#endif

