// $Id: shape.cpp,v 1.7 2014-05-08 18:32:56-07 - - $

#include <typeinfo>
#include <unordered_map>
#include <cmath>
#include <limits>
using namespace std;

#include "shape.h"
#include "util.h"

const double PI  = 3.141592653589793238463;

static unordered_map<void*,string> fontname {
   {GLUT_BITMAP_8_BY_13       , "Fixed-8x13"    },
   {GLUT_BITMAP_9_BY_15       , "Fixed-9x15"    },
   {GLUT_BITMAP_HELVETICA_10  , "Helvetica-10"  },
   {GLUT_BITMAP_HELVETICA_12  , "Helvetica-12"  },
   {GLUT_BITMAP_HELVETICA_18  , "Helvetica-18"  },
   {GLUT_BITMAP_TIMES_ROMAN_10, "Times-Roman-10"},
   {GLUT_BITMAP_TIMES_ROMAN_24, "Times-Roman-24"},
};

static unordered_map<string,void*> fontcode {
   {"Fixed-8x13"    , GLUT_BITMAP_8_BY_13       },
   {"Fixed-9x15"    , GLUT_BITMAP_9_BY_15       },
   {"Helvetica-10"  , GLUT_BITMAP_HELVETICA_10  },
   {"Helvetica-12"  , GLUT_BITMAP_HELVETICA_12  },
   {"Helvetica-18"  , GLUT_BITMAP_HELVETICA_18  },
   {"Times-Roman-10", GLUT_BITMAP_TIMES_ROMAN_10},
   {"Times-Roman-24", GLUT_BITMAP_TIMES_ROMAN_24},
};

ostream& operator<< (ostream& out, const vertex& where) {
   out << "(" << where.xpos << "," << where.ypos << ")";
   return out;
}

shape::shape() {
   DEBUGF ('c', this);
}

text::text (const string& fontname, const string& textdata):
            textdata(textdata) {
      
      DEBUGF ('c', this);
      auto glut_bitmap_font_itor = fontcode.find(fontname);
      if (glut_bitmap_font_itor == fontcode.end())
        throw runtime_error (fontname + ": no such font");
      glut_bitmap_font = glut_bitmap_font_itor->second;
}

ellipse::ellipse (GLfloat width, GLfloat height):
dimension ({width, height}) {
   DEBUGF ('c', this);
}

circle::circle (GLfloat diameter): ellipse (diameter, diameter) {
   DEBUGF ('c', this);
}


polygon::polygon (const vector<vertex>& vertices): vertices(vertices) {
   DEBUGF ('c', this);
   normalize_vertices();
}

rectangle::rectangle (GLfloat width, GLfloat height):
            polygon({}) {
   DEBUGF ('c', this << "(" << width << "," << height << ")");
   vertices.push_back({0, 0});
   vertices.push_back({width, 0});
   vertices.push_back({width, height});
   vertices.push_back({0, height});
   normalize_vertices();
}

square::square (GLfloat width): rectangle (width, width) {
   DEBUGF ('c', this);
}

diamond::diamond (GLfloat width, GLfloat height):
            polygon({}) {
   DEBUGF ('c', this << "(" << width << "," << height << ")");
   vertices.push_back({0, height / 2});
   vertices.push_back({-width / 2, 0});
   vertices.push_back({0, -height / 2});
   vertices.push_back({width / 2, 0});
}

triangle::triangle (const vector<vertex>& vertices): polygon(vertices) {
    // Assignment spec says this needs to be here, so... yeah
}

isosceles::isosceles (GLfloat width, GLfloat height):
    triangle({}) {
    vertices.push_back({0, height});
    vertices.push_back({-width / 2, 0});
    vertices.push_back({width / 2, 0});
    normalize_vertices();
}

equilateral::equilateral (GLfloat width):
    isosceles(width, sqrt(width * width - (width / 2) * (width / 2))) {
    // This space intentionally left blank
}

right_triangle::right_triangle (GLfloat width, GLfloat height):
    triangle({}) {
    vertices.push_back({0, 0});
    vertices.push_back({width, 0});
    vertices.push_back({0, height});
    normalize_vertices();
}

void text::draw (const vertex& center, const rgbcolor& color) const {
   DEBUGF ('d', this << "(" << center << "," << color << ")");
   
   glRasterPos2f(center.xpos, center.ypos);
   glColor3ub(color.red, color.green, color.blue);
   
   // Hopefully this cast is okay - I mean I think it is...
   glutBitmapString(glut_bitmap_font,
        reinterpret_cast<const unsigned char*>(textdata.c_str()));
}

void ellipse::draw (const vertex& center, const rgbcolor& color) const {
   DEBUGF ('d', this << "(" << center << "," << color << ")");
   
   glColor3ub(color.red, color.green, color.blue);
   
   glBegin(GL_LINE_LOOP);
   
   for(int i=0;i<360;i++)
   {
      float rad = (float)i * PI / 180.0f;
      glVertex2f(cos(rad)*dimension.xpos + center.xpos,
                   sin(rad)*dimension.ypos + center.ypos);
   }

   glEnd();
}

void polygon::draw (const vertex& center, const rgbcolor& color) const {
   DEBUGF ('d', this << "(" << center << "," << color << ")");
   
   glColor3ub(color.red, color.green, color.blue);
   
   glBegin(GL_LINE_LOOP);
   for(auto i : vertices)
   {
      glVertex2f(i.xpos + center.xpos, i.ypos + center.ypos);
   }

   glEnd();
}

void polygon::normalize_vertices() {
    vector<vertex> orig_vertices = vertices;
    vertices = vector<vertex>();
    vertices.reserve(orig_vertices.size());
    
    GLfloat total_x = 0;
    GLfloat total_y = 0;
    
    for (unsigned int i = 0; i < orig_vertices.size(); i++) {
        total_x += orig_vertices[i].xpos;
        total_y += orig_vertices[i].ypos;
    }
    
    GLfloat avg_x = total_x / orig_vertices.size();
    GLfloat avg_y = total_y / orig_vertices.size();
    
    for (unsigned int i = 0; i < orig_vertices.size(); i++) {
        vertices.push_back({orig_vertices[i].xpos - avg_x,
            orig_vertices[i].ypos - avg_y});
    }
}

void shape::show (ostream& out) const {
   out << this << "->" << demangle (*this) << ": ";
}

void text::show (ostream& out) const {
   shape::show (out);
   out << glut_bitmap_font << "(" << fontname[glut_bitmap_font]
       << ") \"" << textdata << "\"";
}

void ellipse::show (ostream& out) const {
   shape::show (out);
   out << "{" << dimension << "}";
}

void polygon::show (ostream& out) const {
   shape::show (out);
   out << "{" << vertices << "}";
}

ostream& operator<< (ostream& out, const shape& obj) {
   obj.show (out);
   return out;
}

