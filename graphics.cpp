// $Id: graphics.cpp,v 1.11 2014-05-15 16:42:55-07 - - $

#include <iostream>
using namespace std;

#include <GL/freeglut.h>

#include "graphics.h"
#include "util.h"

int window::width = 640; // in pixels
int window::height = 480; // in pixels
GLfloat window::moveDelta = 4; // in pixels
GLfloat window::borderSize = 4;
rgbcolor window::borderColor = rgbcolor(255, 0, 0);
vector<object> window::objects;
size_t window::selected_obj = 0;
mouse window::mus;

// Executed when window system signals to shut down.
void window::close() {
   DEBUGF ('g', sys_info::execname() << ": exit ("
           << sys_info::exit_status() << ")");
   exit (sys_info::exit_status());
}

// Executed when mouse enters or leaves window.
void window::entry (int mouse_entered) {
   DEBUGF ('g', "mouse_entered=" << mouse_entered);
   window::mus.entered = mouse_entered;
   if (window::mus.entered == GLUT_ENTERED) {
      DEBUGF ('g', sys_info::execname() << ": width=" << window::width
           << ", height=" << window::height);
   }
   glutPostRedisplay();
}

// Called to display the objects in the window.
void window::display() {
   glClear (GL_COLOR_BUFFER_BIT);
   objects[selected_obj].drawBorder(borderColor, borderSize);
   for (auto& object: window::objects) object.draw();
   mus.draw();
   glutSwapBuffers();
}

// Called when window is opened and when resized.
void window::reshape (int width, int height) {
   DEBUGF ('g', "width=" << width << ", height=" << height);
   window::width = width;
   window::height = height;
   glMatrixMode (GL_PROJECTION);
   glLoadIdentity();
   gluOrtho2D (0, window::width, 0, window::height);
   glMatrixMode (GL_MODELVIEW);
   glViewport (0, 0, window::width, window::height);
   glClearColor (0.25, 0.25, 0.25, 1.0);
   glutPostRedisplay();
}


// Executed when a regular keyboard key is pressed.
enum {BS=8, TAB=9, ESC=27, SPACE=32, DEL=127};
void window::keyboard (GLubyte key, int x, int y) {
   DEBUGF ('g', "key=" << (unsigned)key << ", x=" << x << ", y=" << y);
   window::mus.set (x, y);
   unsigned int num = key - 48;
   switch (key) {
      case 'Q': case 'q': case ESC:
         window::close();
         break;
      case 'H': case 'h':
         move_selected_object (LEFT);
         break;
      case 'J': case 'j':
         move_selected_object (DOWN);
         break;
      case 'K': case 'k':
         move_selected_object (UP);
         break;
      case 'L': case 'l':
         move_selected_object (RIGHT);
         break;
      case 'N': case 'n': case SPACE: case TAB:
         if (selected_obj >= objects.size() - 1) selected_obj = 0;
            else selected_obj++;
         break;
      case 'P': case 'p': case BS:
         if (selected_obj == 0) selected_obj = objects.size() - 1;
            else selected_obj--;
         break;
      case '0'...'9':
         if (num < objects.size()) selected_obj = num;
            else cerr << num << ": invalid object" << endl;
         break;
      default:
         cerr << (unsigned)key << ": invalid keystroke" << endl;
         break;
   }
   glutPostRedisplay();
}


// Executed when a special function key is pressed.
void window::special (int key, int x, int y) {
   DEBUGF ('g', "key=" << key << ", x=" << x << ", y=" << y);
   window::mus.set (x, y);
   switch (key) {
      case GLUT_KEY_LEFT: //move_selected_object (-1, 0); 
        move_selected_object (LEFT);
        break;
      case GLUT_KEY_DOWN: //move_selected_object (0, -1); 
        move_selected_object (DOWN);
        break;
      case GLUT_KEY_UP: //move_selected_object (0, +1);
        move_selected_object (UP);
        break;
      case GLUT_KEY_RIGHT: //move_selected_object (+1, 0);
        move_selected_object (RIGHT);
        break;
      case GLUT_KEY_F1: //select_object (1);
        if (1 < objects.size()) selected_obj = 1;
            else cerr << 1 << ": invalid object" << endl;
        break;
      case GLUT_KEY_F2: //select_object (2);
        if (2 < objects.size()) selected_obj = 2;
            else cerr << 2 << ": invalid object" << endl;
        break;
      case GLUT_KEY_F3: //select_object (3);
        if (3 < objects.size()) selected_obj = 3;
            else cerr << 3 << ": invalid object" << endl;
        break;
      case GLUT_KEY_F4: //select_object (4);
        if (4 < objects.size()) selected_obj = 4;
            else cerr << 4 << ": invalid object" << endl;
        break;
      case GLUT_KEY_F5: //select_object (5);
        if (5 < objects.size()) selected_obj = 5;
            else cerr << 5 << ": invalid object" << endl;
        break;
      case GLUT_KEY_F6: //select_object (6);
        if (6 < objects.size()) selected_obj = 6;
            else cerr << 6 << ": invalid object" << endl;
        break;
      case GLUT_KEY_F7: //select_object (7);
        if (7 < objects.size()) selected_obj = 7;
            else cerr << 7 << ": invalid object" << endl;
        break;
      case GLUT_KEY_F8: //select_object (8);
        if (8 < objects.size()) selected_obj = 8;
            else cerr << 8 << ": invalid object" << endl;
        break;
      case GLUT_KEY_F9: //select_object (9);
        if (9 < objects.size()) selected_obj = 9;
            else cerr << 9 << ": invalid object" << endl;
        break;
      case GLUT_KEY_F10: //select_object (10);
        if (10 < objects.size()) selected_obj = 10;
            else cerr << 10 << ": invalid object" << endl;
        break;
      case GLUT_KEY_F11: //select_object (11);
        if (11 < objects.size()) selected_obj = 11;
            else cerr << 11 << ": invalid object" << endl;
        break;
      case GLUT_KEY_F12: //select_object (12);
        if (12 < objects.size()) selected_obj = 12;
            else cerr << 12 << ": invalid object" << endl;
        break;
      default:
         cerr << (unsigned)key << ": invalid function key" << endl;
         break;
   }
   glutPostRedisplay();
}


void window::motion (int x, int y) {
   DEBUGF ('g', "x=" << x << ", y=" << y);
   window::mus.set (x, y);
   glutPostRedisplay();
}

void window::passivemotion (int x, int y) {
   DEBUGF ('g', "x=" << x << ", y=" << y);
   window::mus.set (x, y);
   glutPostRedisplay();
}

void window::mousefn (int button, int state, int x, int y) {
   DEBUGF ('g', "button=" << button << ", state=" << state
           << ", x=" << x << ", y=" << y);
   window::mus.state (button, state);
   window::mus.set (x, y);
   glutPostRedisplay();
}

void window::main () {
   static int argc = 0;
   glutInit (&argc, nullptr);
   glutInitDisplayMode (GLUT_RGBA | GLUT_DOUBLE);
   glutInitWindowSize (window::width, window::height);
   glutInitWindowPosition (128, 128);
   glutCreateWindow (sys_info::execname().c_str());
   glutCloseFunc (window::close);
   glutEntryFunc (window::entry);
   glutDisplayFunc (window::display);
   glutReshapeFunc (window::reshape);
   glutKeyboardFunc (window::keyboard);
   glutSpecialFunc (window::special);
   glutMotionFunc (window::motion);
   glutPassiveMotionFunc (window::passivemotion);
   glutMouseFunc (window::mousefn);
   DEBUGF ('g', "Calling glutMainLoop()");
   glutMainLoop();
}

void window::move_selected_object(Direction d) {
    vertex delta;
    if (d == UP) {
        delta.xpos = 0;
        delta.ypos = 1;
    } else if (d == LEFT) {
        delta.xpos = -1;
        delta.ypos = 0;
    } else if (d == DOWN) {
        delta.xpos = 0;
        delta.ypos = -1;
    } else {
        // d == RIGHT
        delta.xpos = 1;
        delta.ypos = 0;
    }
    
    objects[selected_obj].move(delta.xpos * moveDelta,
        delta.ypos * moveDelta);
}


void mouse::state (int button, int state) {
   switch (button) {
      case GLUT_LEFT_BUTTON: left_state = state; break;
      case GLUT_MIDDLE_BUTTON: middle_state = state; break;
      case GLUT_RIGHT_BUTTON: right_state = state; break;
   }
}

void mouse::draw() {
   static rgbcolor color ("green");
   ostringstream text;
   text << "(" << xpos << "," << window::height - ypos << ")";
   if (left_state == GLUT_DOWN) text << "L"; 
   if (middle_state == GLUT_DOWN) text << "M"; 
   if (right_state == GLUT_DOWN) text << "R"; 
   if (entered == GLUT_ENTERED) {
      void* font = GLUT_BITMAP_HELVETICA_18;
      glColor3ubv (color.ubvec);
      glRasterPos2i (10, 10);
      glutBitmapString (font, (GLubyte*) text.str().c_str());
   }
}

