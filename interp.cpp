// $Id: interp.cpp,v 1.18 2015-02-19 16:50:37-08 - - $

#include <memory>
#include <string>
#include <vector>
using namespace std;

#include <GL/freeglut.h>

#include "debug.h"
#include "interp.h"
#include "shape.h"
#include "util.h"

unordered_map<string,interpreter::interpreterfn>
interpreter::interp_map {
   {"define" , &interpreter::do_define    },
   {"draw"   , &interpreter::do_draw      },
   {"border" , &interpreter::set_border   },
   {"moveby" , &interpreter::set_moveby   },
};

unordered_map<string,interpreter::factoryfn>
interpreter::factory_map {
   {"text"       , &interpreter::make_text              },
   {"ellipse"    , &interpreter::make_ellipse           },
   {"circle"     , &interpreter::make_circle            },
   {"polygon"    , &interpreter::make_polygon           },
   {"rectangle"  , &interpreter::make_rectangle         },
   {"square"     , &interpreter::make_square            },
   {"diamond"    , &interpreter::make_diamond           },
   {"triangle"   , &interpreter::make_triangle          },
   {"isosceles"  , &interpreter::make_isosceles         },
   {"equilateral", &interpreter::make_equilateral       },
   {"right_triangle", &interpreter::make_right_triangle },
   
};

interpreter::shape_map interpreter::objmap;

interpreter::~interpreter() {
   for (const auto& itor: objmap) {
      cout << "objmap[" << itor.first << "] = "
           << *itor.second << endl;
   }
}

void interpreter::interpret (const parameters& params) {
   DEBUGF ('i', params);
   param begin = params.cbegin();
   string command = *begin;
   auto itor = interp_map.find (command);
   if (itor == interp_map.end()) throw runtime_error ("syntax error");
   interpreterfn func = itor->second;
   func (++begin, params.cend());
}

void interpreter::do_define (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   string name = *begin;
   objmap.emplace (name, make_shape (++begin, end));
}


void interpreter::do_draw (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   object o;
   
   string colorname = *begin;
   o.color = string_to_color(colorname);
   
   if (end - begin != 4) throw runtime_error ("syntax error");
   string name = begin[1];
   shape_map::const_iterator itor = objmap.find (name);
   if (itor == objmap.end()) {
      throw runtime_error (name + ": no such shape");
   }
   vertex where {from_string<GLfloat> (begin[2]),
                 from_string<GLfloat> (begin[3])};
   
   o.pshape = itor->second;
   o.center = where;
   window::push_back(o);
}

shape_ptr interpreter::make_shape (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   string type = *begin++;
   
   auto itor = factory_map.find(type);
   if (itor == factory_map.end()) {
      throw runtime_error (type + ": no such shape");
   }
   factoryfn func = itor->second;
   return func (begin, end);
}

shape_ptr interpreter::make_text (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   
   auto i = begin;
   string fontname = *i;
   i++;
   
   stringstream textconts;
   while(i != end) {
        textconts << *i;
        i++;
        if (i != end) textconts << " ";
   }
   
   return make_shared<text> (fontname, textconts.str());
}

shape_ptr interpreter::make_ellipse (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   auto i = begin;
   auto width = *i;
   i++;
   auto height = *i;
   return make_shared<ellipse> (from_string<GLfloat> (width), 
        from_string<GLfloat> (height));
}

shape_ptr interpreter::make_circle (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   string diameter = *begin;
   return make_shared<circle> (from_string<GLfloat> (diameter));
}

shape_ptr interpreter::make_polygon (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   vector<vertex> vertices;
   
   for (auto i = begin; i != end; i++) {
        vertices.push_back(vertex{from_string<GLfloat>(*i),
            from_string<GLfloat>(*++i)});
   }
   
   return make_shared<polygon> (vertices);
}

shape_ptr interpreter::make_rectangle (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   auto i = begin;
   auto width = *i;
   i++;
   auto height = *i;
   
   return make_shared<rectangle> (from_string<GLfloat> (width), 
   from_string<GLfloat> (height));
}

shape_ptr interpreter::make_square (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   return make_shared<square> (from_string<GLfloat> (*begin));
}

shape_ptr interpreter::make_diamond (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   auto i = begin;
   auto width = *i;
   i++;
   auto height = *i;
   return make_shared<diamond> (from_string<GLfloat> (width), 
        from_string<GLfloat> (height));
}

shape_ptr interpreter::make_triangle (param begin, param end) {
   return make_polygon(begin, end);
}

shape_ptr interpreter::make_isosceles (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   auto i = begin;
   auto width = *i;
   i++;
   auto height = *i;
   return make_shared<isosceles> (from_string<GLfloat> (width), 
        from_string<GLfloat> (height));
}

shape_ptr interpreter::make_equilateral (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   return make_shared<equilateral> (from_string<GLfloat> (*begin));
}

shape_ptr interpreter::make_right_triangle (param begin, param end) {
   DEBUGF ('f', range (begin, end));
   auto i = begin;
   auto width = *i;
   i++;
   auto height = *i;
   return make_shared<right_triangle> (from_string<GLfloat> (width), 
        from_string<GLfloat> (height));
}

void interpreter::set_border (param begin, param end) {
   (void)end;
   auto i = begin;
   string colorname = *i;
   i++;
   string borderWidth = *i;
   
   window::set_border(string_to_color(colorname),
        from_string<GLfloat> (borderWidth));
}

void interpreter::set_moveby (param begin, param end) {
   (void)end;
   window::set_moveby(from_string<GLfloat> (*begin));
}

rgbcolor interpreter::string_to_color(string colorname) {
    if (colorname.substr(0, 2) == "0x") {
       // Hex color
       int x = stoi(colorname, nullptr, 16);
       GLubyte red = ((x >> 16) & 0xFF);
       GLubyte green = ((x >> 8) & 0xFF);
       GLubyte blue = ((x) & 0xFF);
       return rgbcolor(red, green, blue);
   } else {
       // Named color
       auto coloritor = color_names.find(colorname);
       if (coloritor == color_names.end()) {
          throw runtime_error (colorname + ": no such color");
       }
       return coloritor->second;
   }
}

